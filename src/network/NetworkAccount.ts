import {
  getAuth,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  signOut,
  indexedDBLocalPersistence,
  initializeAuth,
} from 'firebase/auth';
import { Capacitor } from '@capacitor/core';
import { getDatabase, ref, set, child, get } from 'firebase/database';

import { useTodoStore } from '@/store/todoItems';

import { app } from '@/helpers/FirebaseCore';

if (Capacitor.isNativePlatform()) {
  initializeAuth(app, {
    persistence: indexedDBLocalPersistence,
  });
}

// Initialize Firebase Authentication and get a reference to the service
const auth = getAuth(app);

class NetworkSettings {
  isLoggedIn(): boolean {
    return !(auth.currentUser == null);
  }
  registeraccount(email: string, password: string) {
    return createUserWithEmailAndPassword(auth, email, password);
  }
  login(email: string, password: string) {
    return signInWithEmailAndPassword(auth, email, password);
  }
  logout() {
    return signOut(auth);
  }
  getAccountName() {
    return auth.currentUser?.email ?? '--';
  }
  getAccountId() {
    return auth.currentUser?.uid ?? '';
  }
  async deleteUser(email: string, password: string) {
    await this.login(email, password);
    await auth.currentUser?.delete();
  }
  async syncTasks() {
    const todoStore = useTodoStore();
    const usersroot = 'todousers';

    const userId = this.getAccountId();
    if (userId == '') {
      throw Error('No user ID');
    }

    // READ
    const dbRef = ref(getDatabase());
    const snapshot = await get(child(dbRef, `${usersroot}/${userId}`));

    if (snapshot.exists()) {
      const recData = snapshot.val();
      const tasks = recData.tasks;
      const historicalTaskData = recData.tasksTimestamps;

      if (tasks != null) {
        todoStore.syncTasksFromServer(tasks);
      }

      if (historicalTaskData != null) {
        todoStore.syncTasksHistoricalDataFromServer(historicalTaskData);
      }
    }

    // WRITE
    const db = getDatabase();
    await set(ref(db, usersroot + '/' + userId), {
      tasks: todoStore.todoItems,
      tasksTimestamps: todoStore.todoItemsDone,
    });
  }
}

export default new NetworkSettings();
