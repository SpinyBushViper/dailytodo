import { alertController } from '@ionic/vue';

export function createAlert(
  headerStr = 'Please wait',
  subHeaderStr = 'Network Access',
  buttons = []
) {
  return alertController.create({
    backdropDismiss: false,
    header: headerStr,
    subHeader: subHeaderStr,
    buttons: buttons,
  });
}
