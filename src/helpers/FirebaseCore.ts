import { initializeApp } from 'firebase/app';
import { firebaseConfig } from '../options';

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
