import { getAnalytics, logEvent } from 'firebase/analytics';
import { app } from '@/helpers/FirebaseCore';

const analytics = getAnalytics(app);

// google analytics examples here https://firebase.google.com/docs/analytics/user-properties?hl=en&authuser=0&platform=web

export function analyticsLog(eventString: string) {
  //logEvent(analytics, 'notification_received');
  logEvent(analytics, eventString);
  //logEvent(analytics, 'goal_completion', { name: 'lever_puzzle'});
}
