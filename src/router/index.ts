import { createRouter, createWebHistory } from '@ionic/vue-router';
import { RouteRecordRaw } from 'vue-router';
import HomePage from '../views/HomePage.vue';
import AddPage from '../views/AddPage.vue';
import AddPageCustom from '../views/AddPageCustom.vue';
import EditPage from '../views/EditPage.vue';
import UserAccountPage from '../views/UserAccountPage.vue';
import AboutPage from '../views/AboutPage.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/home',
  },
  {
    path: '/home',
    name: 'Home',
    component: HomePage,
  },
  {
    path: '/add',
    name: 'Add',
    component: AddPage,
  },
  {
    path: '/add/custom',
    name: 'AddCustom',
    component: AddPageCustom,
  },
  {
    path: '/edit/:id',
    name: 'Edit',
    component: EditPage,
    props: true,
  },
  {
    path: '/user',
    name: 'UserAccount',
    component: UserAccountPage,
  },
  {
    path: '/about',
    name: 'About',
    component: AboutPage,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
