import { defineStore } from 'pinia';
import { TodoItem, useTodoStore } from '@/store/todoItems';

export type AddTaskItem = {
  name: string;
  subtitle: string;
  taskName: string;
  selected: boolean;
};

export const useAddTaskStore = defineStore({
  id: 'addTaskStore',
  state: () => ({
    addTaskItems: [
      {
        name: 'Physical: Stretch',
        subtitle: 'Touch your toes for 2 minutes every day',
        taskName: 'Touch your toes for 2 minutes',
        selected: false,
      },
      {
        name: 'Physical: Stretch',
        subtitle: 'Do the splits for 2 minutes every day',
        taskName: 'Do the splits for 2 minutes',
        selected: false,
      },
      {
        name: 'Physical: Power',
        subtitle: 'Plank for 2 minutes every day',
        taskName: 'Plank for 2 minutes',
        selected: false,
      },
      {
        name: 'Physical: Power',
        subtitle: 'Do 10 pressups every day',
        taskName: '10 pressups',
        selected: false,
      },
      {
        name: 'Physical: Power',
        subtitle: 'Do 20 pressups every day',
        taskName: '20 pressups',
        selected: false,
      },
      {
        name: 'Wellbeing: Gratitude',
        subtitle: 'Write down 10 things you are grateful for every day',
        taskName: 'Write down 10 things you are grateful for',
        selected: false,
      },
      {
        name: 'Wellbeing',
        subtitle: 'Meditate for 10 minutes every day',
        taskName: 'Meditate for 10 minutes',
        selected: false,
      },
      {
        name: 'Productivity',
        subtitle: 'Write a blog post every day',
        taskName: 'Write a blog post',
        selected: false,
      },
      {
        name: 'Skill Up',
        subtitle: 'Look up a youtube in your field of expertise every day',
        taskName: 'Look up a youtube in your field of expertise',
        selected: false,
      },
      {
        name: 'Skill Up',
        subtitle: 'Read a non-fiction book for 20 minutes every day',
        taskName: 'Read a non-fiction book for 20 minutes',
        selected: false,
      },
      {
        name: 'Language Learning',
        subtitle: 'Study a foreign language for 10 minutes every day',
        taskName: 'Study a foreign language for 10 minutes',
        selected: false,
      },
      {
        name: 'Keep in Touch',
        subtitle:
          "Call/Text a friend you haven't talked to for a while, every day",
        taskName: 'Call/Text a friend',
        selected: false,
      },
    ] as AddTaskItem[],
  }),
  getters: {
    addTasksSelectedCount(): number {
      return this.addTaskItems.reduce((acc, val: AddTaskItem) => {
        const newVal = val.selected ? 1 : 0;
        return acc + newVal;
      }, 0);
    },
  },
  actions: {
    resetAddTaskItems() {
      this.addTaskItems.forEach((item) => {
        item.selected = false;
      });
    },
    toggleAddItemSelected(item: AddTaskItem) {
      const prevSelected = item.selected;
      item.selected = !prevSelected;
    },
    addSelectedItems() {
      const todoStore = useTodoStore();
      this.addTaskItems
        .filter((item) => item.selected)
        .forEach((item) => {
          const newItem: TodoItem = {
            id: '-1',
            name: item.taskName,
            desc: '',
            storyPoints: 1,
            everyXDays: 1,
            score: -999,
            lastDone: -1,
            nextDue: -1,
            lastUpdatedUTC: 0,
            authorId: '',
            deleted: false,
          };

          todoStore.addTodoItem(newItem);
        });
      this.resetAddTaskItems();
    },
  },
});
