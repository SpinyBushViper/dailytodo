import { defineStore } from 'pinia';
import network from '../network/NetworkAccount';
import { firebaseConfig } from '../options';

export type TodoItem = {
  id: string;
  name: string;
  desc: string;
  storyPoints: number;
  everyXDays: number;
  lastDone: number;
  nextDue: number;
  score: number;
  lastUpdatedUTC: number;
  authorId: string;
  deleted: boolean;
};

type TodoItemDone = {
  done: boolean;
};

interface DoneListObject {
  [timeStampEpoch: string]: TodoItemDone;
}

interface TopLevelDoneListObject {
  [taskId: string]: DoneListObject;
}

interface AnalyticsObject {
  timesDone: number;
  elapsedDays: number;
  score: number;
  visualDisplay: string;
}

interface AnalyticsObjectOneTask {
  timeSpans: AnalyticsObject[];
  finalOverview: string;
}

interface TopLevelAnalyticsObject {
  [taskId: string]: AnalyticsObjectOneTask;
}

export const useTodoStore = defineStore({
  id: 'todoStore',
  state: () => ({
    todoItems: [] as TodoItem[],
    todoItemsDone: {} as TopLevelDoneListObject,
    analyticsItems: {} as TopLevelAnalyticsObject,
    shouldShowLoginTask: false,
  }),
  getters: {
    todoCount(): number {
      return this.todoItems.length;
    },
  },
  actions: {
    updateIsLoggedIn() {
      this.shouldShowLoginTask = false;
      setTimeout(() => {
        const activated = firebaseConfig.apiKey.length > 3;
        const loggedIn = network.isLoggedIn();

        this.shouldShowLoginTask = activated && !loggedIn;
      }, 5000);
    },
    todoWithId(_id: string): TodoItem | null {
      const i = this.todoItems.findIndex((s) => s.id === _id);
      if (i >= -1) {
        return this.todoItems[i];
      }
      return null;
    },
    addTodoItem(item: TodoItem) {
      const nowDate = this.localDateNow();
      const rnd = Math.floor(Math.random() * 8999) + 1000; // a random number between 1000 and 9999
      item.id = rnd + '-' + nowDate;
      item.authorId = network.getAccountId();
      this.todoItems.push(item);
    },
    syncTasksFromServer(serverSideTasks: TodoItem[]) {
      // Take the incoming tasks from the server and add or update to local list if necessary
      serverSideTasks.forEach((serverTask) => {
        const localMatchingTask = this.todoWithId(serverTask.id);
        if (localMatchingTask == null) {
          // Does not exist locally, add to local
          this.todoItems.push(serverTask);
        } else if (
          serverTask.lastUpdatedUTC > localMatchingTask.lastUpdatedUTC
        ) {
          // exists locally, but Server side task is newer than local.
          // replace local with server task
          this.realDeleteItem(localMatchingTask);
          this.todoItems.push(serverTask);
        }
      });
    },
    syncTasksHistoricalDataFromServer(
      serversideHistoricalTasks: TopLevelDoneListObject
    ) {
      // Take the incoming historical task data from the server and add or update to local list if necessary
      const localTodoItemsIds = Object.keys(this.todoItemsDone);
      Object.keys(serversideHistoricalTasks).forEach((taskId) => {
        if (localTodoItemsIds.includes(taskId)) {
          // add server-side to local existing version
          const newSingleTask = {
            ...this.todoItemsDone[taskId],
            ...serversideHistoricalTasks[taskId],
          };
          this.todoItemsDone[taskId] = newSingleTask;
        } else {
          // create and add to local
          const newTodoItemsDone = {
            ...this.todoItemsDone,
            [taskId]: serversideHistoricalTasks[taskId],
          };
          this.todoItemsDone = newTodoItemsDone;
        }
      });
    },
    refreshTaskAuthorIds() {
      // call this after logging in. There may be new tasks created before logging in
      const currentAccountId = network.getAccountId();
      if (currentAccountId == '') {
        return; // Not logged yet, don't do anything
      }
      this.todoItems.forEach((item) => {
        // set authorId if it isn't set yet
        if (
          item.authorId == '' ||
          item.authorId == null ||
          item.authorId == undefined
        ) {
          item.authorId = currentAccountId;
          item.lastUpdatedUTC = Date.now();
        } else if (item.authorId != currentAccountId) {
          // remove the tasks that do not belong to this logged in user. This could happen because a different user logged in
          this.realDeleteItem(item);
        }
      });
    },
    removeallTodoItems() {
      // This will be needed when user logs out. Necessary because it might be possible to clone other people's tasks if user logs back in as someone else later on
      this.todoItems = [];
      this.todoItemsDone = {};
      this.analyticsItems = {};
      this.shouldShowLoginTask = false;
    },
    realDeleteItem(item: TodoItem) {
      const i = this.todoItems.findIndex((s) => s.id === item.id);
      if (i >= -1) {
        this.todoItems.splice(i, 1);
      }
    },
    removeTodoItem(item: TodoItem) {
      // Mark deleted. Do not delete so it is saved online on the next sync
      item.deleted = true;
      item.lastUpdatedUTC = Date.now();
    },
    updateTask(
      _id: string,
      name: string,
      description: string,
      storyPoints: number,
      everyXDays: number
    ) {
      const i = this.todoItems.findIndex((s) => s.id === _id);
      if (i >= -1) {
        this.todoItems[i].name = name;
        this.todoItems[i].desc = description;
        this.todoItems[i].storyPoints = storyPoints;
        this.todoItems[i].everyXDays = everyXDays;
        this.todoItems[i].lastUpdatedUTC = Date.now();
      }
    },
    doneToday(item: TodoItem) {
      const i = this.todoItems.findIndex((s) => s.id === item.id);
      if (i >= 0) {
        const localDateStamp = this.localDateNow();
        this.todoItems[i].lastDone = localDateStamp;
        this.todoItems[i].lastUpdatedUTC = Date.now();
        this.todoItemsDone[item.id] = {
          ...this.todoItemsDone[item.id],
          [localDateStamp]: { done: true },
        };
      }
    },
    doneCountForTask(taskId: string): number {
      const todoItemsDoneList = this.todoItemsDone[taskId];
      if (todoItemsDoneList == undefined || todoItemsDoneList == null) {
        return 0;
      }
      return Object.keys(this.todoItemsDone[taskId]).length;
    },
    localDateNow() {
      const localeOffsetMillis = 1000 * 60 * new Date().getTimezoneOffset();
      const now = Date.now();
      const finalTime = now - localeOffsetMillis;

      return finalTime;
    },
    _reorderTasks() {
      // Re-order tasks in order of Scores. This assumes the scores were already calculated

      this.todoItems.sort((a: TodoItem, b: TodoItem) => {
        return a.score < b.score ? -1 : 1;
      });
    },
    _analyticsDataForTaskEveryXDaysSpan(
      taskId: string,
      everyXDays: number,
      spanDays: number,
      earliestDoneEpoch: number
    ): AnalyticsObject {
      const oneDayMillis = 1000 * 60 * 60 * 24;
      const nowMilliseconds = this.localDateNow();
      const currentDayMillis =
        nowMilliseconds - (nowMilliseconds % oneDayMillis);
      const oldestCutOffDay = currentDayMillis - oneDayMillis * spanDays;

      if (earliestDoneEpoch > oldestCutOffDay) {
        // The earliest known done time is after the cut-off point for this time period,
        // So don't show any analysis
        return {
          timesDone: 0,
          elapsedDays: spanDays,
          score: 0,
          visualDisplay: '',
        };
      }

      const doneItems = Object.keys(this.todoItemsDone[taskId]);
      const doneItemsInRange = doneItems.filter((val) => {
        return Number(val) > oldestCutOffDay;
      });

      const timesDone = doneItemsInRange.length;
      const timesGoalForSpan = spanDays / everyXDays;
      const score = timesDone / timesGoalForSpan;

      function dispCharForScore(score: number): string {
        if (score > 0.85) return '🟢';
        if (score > 0.5) return '🟡';
        if (score > 0.2) return '🟠';
        return '🛑';
      }
      const visualDisplay = dispCharForScore(score);

      return {
        timesDone,
        elapsedDays: spanDays,
        score,
        visualDisplay,
      };
    },
    _getLastDoneEpochForTask(taskId: string): number {
      const doneItems = this.todoItemsDone[taskId];
      if (doneItems == null) {
        return 0;
      }

      let earliestEpoch = Number.MAX_VALUE;
      Object.keys(doneItems).forEach((oneDoneEpoch) => {
        const numberOneDoneEpoch = Number(oneDoneEpoch);
        if (numberOneDoneEpoch < earliestEpoch) {
          earliestEpoch = numberOneDoneEpoch;
        }
      });

      return earliestEpoch;
    },
    _analyticsDataForTask(taskId: string): AnalyticsObjectOneTask | null {
      const task = this.todoWithId(taskId);
      if (task == null) {
        return null; // Task not found
      }

      const everyXDaysForTask = task.everyXDays;

      const lastRecordedDoneEpoch = this._getLastDoneEpochForTask(taskId);

      const timeSpans = [
        this._analyticsDataForTaskEveryXDaysSpan(
          taskId,
          everyXDaysForTask,
          7,
          lastRecordedDoneEpoch
        ),
        this._analyticsDataForTaskEveryXDaysSpan(
          taskId,
          everyXDaysForTask,
          14,
          lastRecordedDoneEpoch
        ),
        this._analyticsDataForTaskEveryXDaysSpan(
          taskId,
          everyXDaysForTask,
          30,
          lastRecordedDoneEpoch
        ),
      ];

      const finalOverview = timeSpans.reduce((acc, cur) => {
        return acc + cur.visualDisplay;
      }, '');
      return { timeSpans, finalOverview };
    },
    _generateAnalyticsData() {
      // For each taskId, create data sets for 'analytics' of the history of each task
      this.analyticsItems = {};
      const taskIds = Object.keys(this.todoItemsDone);
      taskIds.forEach((taskId: string) => {
        const newAnalyticsForTask = this._analyticsDataForTask(taskId);
        if (newAnalyticsForTask != null) {
          this.analyticsItems[taskId] = newAnalyticsForTask;
        }
      });
    },
    getAnalysisOverview(taskId: string) {
      const analyticsForTask = this.analyticsItems[taskId];
      if (analyticsForTask == null) {
        return '';
      }
      return analyticsForTask.finalOverview;
    },
    calculateScores() {
      const oneDayMillis = 1000 * 60 * 60 * 24;
      const nowMilliseconds = this.localDateNow();
      const currentDayMillis =
        nowMilliseconds - (nowMilliseconds % oneDayMillis);
      this.todoItems.forEach((element) => {
        const lastDoneWithLocaleOffset = element.lastDone;
        const lastDoneDayMillis =
          lastDoneWithLocaleOffset - (lastDoneWithLocaleOffset % oneDayMillis);
        if (lastDoneDayMillis > 1000) {
          const everyXDaysMillis = element.everyXDays * oneDayMillis;
          const nextDueDayMillis = lastDoneDayMillis + everyXDaysMillis;
          const score =
            ((nextDueDayMillis - currentDayMillis) / oneDayMillis) *
            (1 / element.everyXDays);

          element.score = score;
          element.nextDue = nextDueDayMillis;
        } else {
          element.score = -999;
        }

        // (Score Formula = (NextDue - CurrentDay) * ( 1 / EveryXDays ) , Needs done (RED) if score is <= 0)
      });
      this._generateAnalyticsData();
      this._reorderTasks();
      this.updateIsLoggedIn();
    },
  },
  persist: true,
});
