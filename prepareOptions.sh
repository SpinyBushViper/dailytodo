if [ "$1" == "" ]; then
    echo 'Password Required. Check README.MD'
    exit -1
fi

cd src
unzip -P $1 -o options.zip
